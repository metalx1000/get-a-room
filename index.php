<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" crossorigin="anonymous"></script>

<style>
  #play{
    margin-top: 50px;
    font-size: 50px;
    padding: 75px;
  }
</style>
<script src="libs/howler.core.js"></script>
<script>
var sounds = [];
    for (var i=1;i<17;i++){
     sounds.push(new Howl({src: ['audio/' + i + '.mp3']}));
    }

      $(document).ready(function(){
        $("#play").click(function(){
          var i = Math.floor((Math.random() * 16));
          sounds[i].play();
        });

      });
    </script>
  </head>
  <body>

    <div id="main" class="container">
      <div class="row">
          <button id="play" type="button" class="btn btn-primary btn-block">Click Me</button>
      </div>
    </div>

  </body>
</html>

